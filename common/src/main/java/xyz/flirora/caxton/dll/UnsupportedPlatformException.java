// (C) 2022-2023 +merlan #flirora.
// SPDX-License-Identifier: MIT
package xyz.flirora.caxton.dll;

public class UnsupportedPlatformException extends RuntimeException {

  public UnsupportedPlatformException(String s) {
    super(s);
  }
}
