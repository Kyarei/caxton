// (C) 2022-2023 +merlan #flirora.
// SPDX-License-Identifier: MIT
package xyz.flirora.caxton.dll;

import java.nio.ByteBuffer;
import xyz.flirora.caxton.command.DebugShapeInfo;
import xyz.flirora.caxton.layout.ShapingResult;

/** Internal methods implemented by the {@code caxton-impl} Rust crate. */
public class CaxtonInternal {
  // This takes options as a string in order to simplify handling on the Rust side.
  public static native long createFont(ByteBuffer fontData, String cachePath, String options);

  public static native void destroyFont(long addr);

  public static native int fontGlyphIndex(long addr, int codePoint);

  public static native short[] fontMetrics(long addr);

  public static native int fontMipmapLayers(long addr);

  public static native int fontAtlasSize(long addr);

  public static native int fontAtlasPhysicalSize(long addr);

  public static native long fontAtlasLocations(long addr, int mipmap);

  public static native long fontBboxes(long addr);

  public static native long fontAtlasPage(long addr, int mipmap);

  public static native int fontBytesPerPixel(long addr);

  public static native String fontGlyphName(long addr, int glyphId);

  public static native DebugShapeInfo fontShapeForDebug(long addr, String s);

  public static native long configureFont(long fontAddr, String settings);

  public static native void destroyConfiguredFont(long addr);

  public static native ShapingResult[] shape(long fontAddr, char[] s, int[] bidiRuns);
}
