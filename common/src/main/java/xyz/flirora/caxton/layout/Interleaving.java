// (C) 2023 +merlan #flirora.
// SPDX-License-Identifier: MIT
package xyz.flirora.caxton.layout;

/** Constants for strides and offsets of interleaved data. */
public class Interleaving {
  public static final int SHAPING_RUN_STRIDE = 4;
  public static final int SHAPING_RUN_OFF_START = 0;
  public static final int SHAPING_RUN_OFF_END = 1;
  public static final int SHAPING_RUN_OFF_LEVEL = 2;
  public static final int SHAPING_RUN_OFF_SCRIPT = 3;
}
