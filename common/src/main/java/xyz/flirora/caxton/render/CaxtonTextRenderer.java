// (C) 2022-2024 +merlan #flirora.
// SPDX-License-Identifier: MIT
package xyz.flirora.caxton.render;

import static xyz.flirora.caxton.layout.Interleaving.*;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.logging.LogUtils;
import it.unimi.dsi.fastutil.ints.IntList;
import java.util.function.Supplier;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.*;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.Window;
import net.minecraft.text.OrderedText;
import net.minecraft.text.Style;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.random.Random;
import org.apache.commons.lang3.mutable.MutableFloat;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.slf4j.Logger;
import xyz.flirora.caxton.font.CaxtonFont;
import xyz.flirora.caxton.font.CaxtonFontOptions;
import xyz.flirora.caxton.font.ConfiguredCaxtonFont;
import xyz.flirora.caxton.layout.*;
import xyz.flirora.caxton.mixin.TextRendererAccessor;

@Environment(EnvType.CLIENT)
public class CaxtonTextRenderer {
  // Copied from TextRenderer
  private static final Vector3f FORWARD_SHIFT = new Vector3f(0.0f, 0.0f, 0.03f);
  private static final Logger LOGGER = LogUtils.getLogger();
  private final CaxtonTextHandler handler;
  final TextRenderer vanillaTextRenderer;
  private final Random RANDOM = Random.createLocal();
  private CaxtonGlyphCache cache;
  private final Supplier<CaxtonGlyphCache> cacheSupplier;
  private final CaxtonTextRenderLayers renderLayers;
  public boolean rtl;

  public CaxtonTextRenderer(
      TextRenderer vanillaTextRenderer,
      TextHandler vanillaTextHandler,
      Supplier<CaxtonGlyphCache> cacheSupplier) {
    this.handler = new CaxtonTextHandler(this::getFontStorage, vanillaTextHandler);
    this.vanillaTextRenderer = vanillaTextRenderer;
    this.cacheSupplier = cacheSupplier;
    this.renderLayers = new CaxtonTextRenderLayers();
  }

  public static CaxtonTextRenderer getInstance() {
    return ((HasCaxtonTextRenderer) MinecraftClient.getInstance().textRenderer)
        .getCaxtonTextRenderer();
  }

  public static CaxtonTextRenderer getAdvanceValidatingInstance() {
    return ((HasCaxtonTextRenderer) MinecraftClient.getInstance().advanceValidatingTextRenderer)
        .getCaxtonTextRenderer();
  }

  // Copy of TextRenderer.tweakTransparency
  private static int tweakTransparency(int argb) {
    if ((argb & 0xFC000000) == 0) {
      return argb | 0xFF000000;
    }
    return argb;
  }

  private static float getMinThickness(Matrix4f modelView) {
    // I don’t want to have to deal with computing this for every underlined
    // character
    if (modelView.isAffine()) {
      Window window = MinecraftClient.getInstance().getWindow();
      Vector3f v = new Vector3f(0, 1, 0);
      modelView.transformDirection(v);
      RenderSystem.getProjectionMatrix().transformDirection(v);
      v.mul(window.getWidth() / 2.0f, window.getHeight() / 2.0f, 0);
      float fontUnitYPx = MathHelper.sqrt(v.x * v.x + v.y * v.y);
      return 1.0f / fontUnitYPx;
    }
    return 0.0f;
  }

  public FontStorage getFontStorage(Identifier id) {
    return ((TextRendererAccessor) this.vanillaTextRenderer).callGetFontStorage(id);
  }

  public float drawLayer(
      String text,
      float x,
      float y,
      int color,
      boolean shadow,
      Matrix4f matrix,
      VertexConsumerProvider vertexConsumerProvider,
      TextRenderer.TextLayerType layerType,
      int backgroundColor,
      int light,
      boolean mirror,
      int leftmostCodePoint,
      float maxWidth) {
    CaxtonText runGroups =
        CaxtonText.fromFormatted(
            text, this::getFontStorage, Style.EMPTY, false, this.rtl, handler.getCache());
    float newX =
        drawRunGroups(
            x,
            y,
            color,
            shadow,
            matrix,
            vertexConsumerProvider,
            CaxtonTextLayerType.fromVanilla(layerType),
            backgroundColor,
            light,
            runGroups,
            true,
            leftmostCodePoint,
            maxWidth,
            getMinThickness(matrix));
    if (!shadow) this.rtl = false;
    return newX;
  }

  public float drawLayer(
      OrderedText text,
      float x,
      float y,
      int color,
      boolean shadow,
      Matrix4f matrix,
      VertexConsumerProvider vertexConsumerProvider,
      TextRenderer.TextLayerType layerType,
      int backgroundColor,
      int light,
      boolean mirror,
      int leftmostCodePoint,
      float maxWidth) {
    CaxtonText runGroups =
        CaxtonText.from(text, this::getFontStorage, false, this.rtl, handler.getCache());
    return drawRunGroups(
        x,
        y,
        color,
        shadow,
        matrix,
        vertexConsumerProvider,
        CaxtonTextLayerType.fromVanilla(layerType),
        backgroundColor,
        light,
        runGroups,
        true,
        leftmostCodePoint,
        maxWidth,
        getMinThickness(matrix));
  }

  public void drawWithOutline(
      OrderedText text,
      float x,
      float y,
      int color,
      int outlineColor,
      Matrix4f matrix,
      VertexConsumerProvider vertexConsumers,
      int light,
      boolean addCorners) {
    Threshold NO_THRESHOLD = new Threshold(-1);
    float minThickness = getMinThickness(matrix);

    CaxtonText runGroups =
        CaxtonText.from(text, this::getFontStorage, false, this.rtl, handler.getCache());
    int effectiveOutlineColor = tweakTransparency(outlineColor);
    int effectiveColor = tweakTransparency(color);

    CaxtonTextDrawer outlineDrawer =
        new CaxtonTextDrawer(
            this,
            vertexConsumers,
            0.0f,
            0.0f,
            effectiveOutlineColor,
            false,
            matrix,
            TextRenderer.TextLayerType.NORMAL,
            light);
    CaxtonTextDrawer centralDrawer =
        new CaxtonTextDrawer(
            this,
            vertexConsumers,
            x,
            y,
            effectiveColor,
            false,
            matrix,
            TextRenderer.TextLayerType.POLYGON_OFFSET,
            light);

    for (RunGroup runGroup : runGroups.runGroups()) {
      ConfiguredCaxtonFont font = runGroup.getFont();
      if (font == null) {
        MutableFloat xBox = new MutableFloat();
        for (int dx = -1; dx <= 1; ++dx) {
          for (int dy = -1; dy <= 1; ++dy) {
            if (dx == 0 && dy == 0) continue;
            if (!addCorners && dx != 0 && dy != 0) continue;
            xBox.setValue(x);
            int dxf = dx, dyf = dy;
            runGroup.acceptRender(
                (index, style, codePoint) -> {
                  int index2 = runGroup.getCharOffset() + index;

                  FontStorage fontStorage = this.getFontStorage(style.getFont());
                  Glyph glyph = fontStorage.getGlyph(codePoint, false);
                  float shadowOffset = glyph.getShadowOffset();

                  outlineDrawer.setX(xBox.floatValue() + dxf * shadowOffset);
                  outlineDrawer.setY(y + dyf * shadowOffset);
                  xBox.add(glyph.getAdvance(style.isBold()));
                  return outlineDrawer.accept(index2, style, codePoint);
                });
          }
        }
        centralDrawer.setX(x);
        runGroup.acceptRender(
            (index, style, codePoint) -> {
              int index2 = runGroup.getCharOffset() + index;
              return centralDrawer.accept(index2, style, codePoint);
            });
        x = centralDrawer.getX();
      } else {
        ShapingResult[] shapingResults = runGroup.getShapingResults();

        int outlinedGlyphOffset =
            font.font().getOptions().fontTech().offsetOutlineGlyphsByGlyphCounts()
                ? font.font().getGlyphCount()
                : 0;

        // FIXME (hard): underlines and strikethroughs don’t render
        // properly with this strategy
        for (int i = 0; i < shapingResults.length; i++) {
          ShapingResult shapingResult = shapingResults[i];
          drawShapedRun(
              shapingResult,
              runGroup,
              i,
              x,
              y,
              false,
              matrix,
              vertexConsumers,
              CaxtonTextLayerType.OUTLINE,
              light,
              outlineDrawer,
              NO_THRESHOLD,
              Float.POSITIVE_INFINITY,
              minThickness,
              outlinedGlyphOffset);
          x =
              drawShapedRun(
                  shapingResult,
                  runGroup,
                  i,
                  x,
                  y,
                  false,
                  matrix,
                  vertexConsumers,
                  CaxtonTextLayerType.POLYGON_OFFSET,
                  light,
                  centralDrawer,
                  NO_THRESHOLD,
                  Float.POSITIVE_INFINITY,
                  minThickness,
                  0);
        }
      }
      outlineDrawer.drawLayer(x);
      centralDrawer.drawLayer(x);
    }
  }

  public float draw(
      CaxtonText text,
      float x,
      float y,
      int color,
      boolean shadow,
      Matrix4f matrix,
      VertexConsumerProvider vertexConsumerProvider,
      boolean seeThrough,
      int backgroundColor,
      int light,
      int leftmostCodePoint,
      float maxWidth) {
    float minThickness = getMinThickness(matrix);
    CaxtonTextLayerType layerType =
        seeThrough ? CaxtonTextLayerType.SEE_THROUGH : CaxtonTextLayerType.NORMAL;
    color = tweakTransparency(color);
    Matrix4f matrix4f = new Matrix4f(matrix);
    if (shadow) {
      x =
          this.drawRunGroups(
              x,
              y,
              color,
              true,
              matrix,
              vertexConsumerProvider,
              layerType,
              backgroundColor,
              light,
              text,
              true,
              leftmostCodePoint,
              maxWidth,
              minThickness);
    } else {
      x =
          this.drawRunGroups(
              x,
              y,
              color,
              false,
              matrix4f,
              vertexConsumerProvider,
              layerType,
              backgroundColor,
              light,
              text,
              true,
              leftmostCodePoint,
              maxWidth,
              minThickness);
    }
    return (int) x + (shadow ? 1 : 0);
  }

  private float drawRunGroups(
      float x,
      float y,
      int color,
      boolean shadow,
      Matrix4f matrix,
      VertexConsumerProvider vertexConsumerProvider,
      CaxtonTextLayerType layerType,
      int backgroundColor,
      int light,
      CaxtonText text,
      boolean mirror,
      int leftmostCodePoint,
      float maxWidth,
      float minThickness) {
    // System.err.println(text);
    getCache();
    Threshold threshold = new Threshold(leftmostCodePoint);
    float origX = x;
    float maxX = x + maxWidth;
    CaxtonTextDrawer drawer =
        new CaxtonTextDrawer(
            this,
            vertexConsumerProvider,
            x,
            y,
            color,
            backgroundColor,
            shadow,
            matrix,
            layerType.asVanilla(),
            light,
            mirror);
    for (RunGroup runGroup : text.runGroups()) {
      if (threshold.shouldSkip(runGroup)) {
        continue;
      }
      if (x >= maxX) break;
      if (runGroup.getFont() == null) {
        drawer.setX(x);
        runGroup.acceptRender(
            (index, style, codePoint) -> {
              int index2 = runGroup.getCharOffset() + index;
              if (threshold.updateLegacy(index2)) {
                return true;
              }
              if (drawer.getX() >= maxX + handler.getWidth(codePoint, style)) return false;
              return drawer.accept(index2, style, codePoint);
            });
        x = drawer.getX();
      } else {
        ShapingResult[] shapingResults = runGroup.getShapingResults();

        for (int index = 0; index < shapingResults.length; ++index) {
          ShapingResult shapingResult = shapingResults[index];
          x =
              drawShapedRun(
                  shapingResult,
                  runGroup,
                  index,
                  x,
                  y,
                  shadow,
                  matrix,
                  vertexConsumerProvider,
                  layerType,
                  light,
                  drawer,
                  threshold,
                  maxX,
                  minThickness,
                  0);
        }
      }
    }
    drawer.setX(x);
    drawer.drawLayer(origX);
    return x;
  }

  private float drawShapedRun(
      ShapingResult shapedRun,
      RunGroup runGroup,
      int index,
      float x,
      float y,
      boolean shadow,
      Matrix4f matrix,
      VertexConsumerProvider vertexConsumers,
      CaxtonTextLayerType layerType,
      int light,
      CaxtonTextDrawer drawer,
      Threshold threshold,
      float maxX,
      float minThickness,
      int glyphOffset) {
    if (x >= maxX) return x;

    ConfiguredCaxtonFont configuredFont = runGroup.getFont();
    CaxtonFont font = configuredFont.font();
    CaxtonFontOptions options = font.getOptions();
    CaxtonGlyphCache.Font cacheForFont = cache.forFont(font);

    assert glyphOffset + font.getGlyphCount() <= font.getTlistSize();

    double shrink = options.shrinkage();
    int margin = options.margin();
    float shadowOffset = configuredFont.shadowOffset();
    float textZ = shadow ? 0.03f : 0.0f;
    float pageSize = CaxtonAtlas.PAGE_SIZE;

    int offset = runGroup.getBidiRuns()[SHAPING_RUN_STRIDE * index + SHAPING_RUN_OFF_START];

    int underlinePosition = font.getMetrics(CaxtonFont.Metrics.UNDERLINE_POSITION);
    int underlineThickness = font.getMetrics(CaxtonFont.Metrics.UNDERLINE_THICKNESS);
    int strikeoutPosition = font.getMetrics(CaxtonFont.Metrics.STRIKEOUT_POSITION);
    int strikeoutThickness = font.getMetrics(CaxtonFont.Metrics.STRIKEOUT_THICKNESS);

    float scale = configuredFont.getScale();
    float baselineY = y + 7.0f + configuredFont.shiftY();
    x += configuredFont.shiftX();

    float yu = baselineY - underlinePosition * scale;
    float ys = baselineY - strikeoutPosition * scale;
    float dyu = underlineThickness * scale;
    float dys = strikeoutThickness * scale;
    float ou = 0.5f * dyu, os = 0.5f * dys;

    // I don’t want to have to deal with computing this for every underlined
    // character
    if (dyu < minThickness) dyu = minThickness;
    if (dys < minThickness) dys = minThickness;

    float y0u = yu + 0.5f * dyu;
    float y1u = yu - 0.5f * dyu;
    float y0s = ys + 0.5f * dys;
    float y1s = ys - 0.5f * dys;

    int numGlyphs = shapedRun.numGlyphs();
    int cumulAdvanceX = 0;
    for (int i = 0; i < numGlyphs; ++i) {
      int glyphId = shapedRun.glyphId(i);
      int clusterIndex = shapedRun.clusterIndex(i);

      if (threshold.updateCaxton(runGroup, index, shapedRun, i)) {
        continue;
      }

      Style style = runGroup.getStyleAt(offset + clusterIndex);
      if (style.isObfuscated()) {
        long tlLoc = font.getTlistLocation(glyphId, 0);
        int width = (int) (tlLoc & 0xFFFF);
        IntList others = font.getGlyphsByWidth().get(width);
        glyphId = others.getInt(RANDOM.nextInt(others.size()));
      }
      int textColor = drawer.getTextColor(style.getColor());
      int shadowColor = drawer.getShadowColor(style, textColor);

      int advanceX = shapedRun.advanceX(i);
      int offsetX = shapedRun.offsetX(i);
      int offsetY = shapedRun.offsetY(i);
      int gx = cumulAdvanceX + offsetX;

      long atlasLoc = cacheForFont.getOrCreateAtlasLocation(glyphOffset + glyphId);
      if (atlasLoc != 0) {
        int atlasX = CaxtonAtlas.getX(atlasLoc);
        int atlasY = CaxtonAtlas.getY(atlasLoc);
        int atlasWidth = CaxtonAtlas.getW(atlasLoc);
        int atlasHeight = CaxtonAtlas.getH(atlasLoc);
        int atlasPageIndex = CaxtonAtlas.getPage(atlasLoc);
        CaxtonAtlas.Page atlasPage = cache.getAtlasPageTexture(atlasPageIndex);

        long glyphBbox = font.getBbox(glyphId);
        short bbXMin = (short) glyphBbox;
        short bbYMin = (short) (glyphBbox >> 16);
        short bbXMax = (short) (glyphBbox >> 32);
        short bbYMax = (short) (glyphBbox >> 48);
        int bbWidth = ((int) bbXMax) - ((int) bbXMin);
        int bbHeight = ((int) bbYMax) - ((int) bbYMin);
        gx += bbXMin;
        offsetY += bbYMin;

        RenderLayer renderLayer =
            renderLayers.text(atlasPage, layerType, options.fontTech(), configuredFont.blur());
        VertexConsumer vertexConsumer = vertexConsumers.getBuffer(renderLayer);

        // Draw the quad

        float x0 = (float) (x + (gx - shrink * margin) * scale);
        float y1 = (float) ((-offsetY + shrink * margin) * scale);
        float u0 = atlasX / pageSize;
        float v0 = atlasY / pageSize;
        float x1 = (float) (x + (gx + shrink * (atlasWidth - margin)) * scale);
        float y0 = (float) ((-offsetY - shrink * (atlasHeight - margin)) * scale);
        float u1 = (atlasX + atlasWidth) / pageSize;
        float v1 = (atlasY + atlasHeight) / pageSize;
        float lowerOffset = configuredFont.slant() * y0;
        float upperOffset = configuredFont.slant() * y1;
        y0 += baselineY;
        y1 += baselineY;

        if (x1 >= maxX) break;

        vertexConsumer.vertex(matrix, x0 + lowerOffset, y0, textZ).color(textColor);
        vertexConsumer.texture(u0, v0).light(light);
        vertexConsumer.vertex(matrix, x0 + upperOffset, y1, textZ).color(textColor);
        vertexConsumer.texture(u0, v1).light(light);
        vertexConsumer.vertex(matrix, x1 + upperOffset, y1, textZ).color(textColor);
        vertexConsumer.texture(u1, v1).light(light);
        vertexConsumer.vertex(matrix, x1 + lowerOffset, y0, textZ).color(textColor);
        vertexConsumer.texture(u1, v0).light(light);

        if (shadow) {
          x0 += shadowOffset;
          x1 += shadowOffset;
          y0 += shadowOffset;
          y1 += shadowOffset;
          vertexConsumer.vertex(matrix, x0 + lowerOffset, y0, 0.0f).color(shadowColor);
          vertexConsumer.texture(u0, v0).light(light);
          vertexConsumer.vertex(matrix, x0 + upperOffset, y1, 0.0f).color(shadowColor);
          vertexConsumer.texture(u0, v1).light(light);
          vertexConsumer.vertex(matrix, x1 + upperOffset, y1, 0.0f).color(shadowColor);
          vertexConsumer.texture(u1, v1).light(light);
          vertexConsumer.vertex(matrix, x1 + lowerOffset, y0, 0.0f).color(shadowColor);
          vertexConsumer.texture(u1, v0).light(light);
        }
      }

      float x0a = x + cumulAdvanceX * scale;
      float x1a = x + (cumulAdvanceX + advanceX) * scale;
      if (style.isUnderlined()) {
        drawer.addRectangle(
            new BakedGlyph.Rectangle(
                x0a, y0u, x1a, y1u, drawer.getForegroundZIndex() + textZ, textColor));
        if (shadow) {
          drawer.addRectangle(
              new BakedGlyph.Rectangle(
                  x0a + shadowOffset,
                  y0u + shadowOffset,
                  x1a + shadowOffset,
                  y1u + shadowOffset,
                  drawer.getForegroundZIndex(),
                  shadowColor));
        }
      }
      if (style.isStrikethrough()) {
        drawer.addRectangle(
            new BakedGlyph.Rectangle(
                x0a, y0s, x1a, y1s, drawer.getForegroundZIndex() + textZ, textColor));
        if (shadow) {
          drawer.addRectangle(
              new BakedGlyph.Rectangle(
                  x0a + shadowOffset,
                  y0s + shadowOffset,
                  x1a + shadowOffset,
                  y1s + shadowOffset,
                  drawer.getForegroundZIndex(),
                  shadowColor));
        }
      }

      cumulAdvanceX += advanceX;
    }
    return x + cumulAdvanceX * scale - configuredFont.shiftX();
  }

  public CaxtonGlyphCache getCache() {
    if (cache == null) cache = cacheSupplier.get();
    return cache;
  }

  public void clearCaches() {
    this.handler.clearCaches();
    LOGGER.info("Cleared layout caches");
    if (this.cache != null) this.cache.clear();
    this.renderLayers.clear();
    LOGGER.info("Cleared rendering caches");
  }

  public CaxtonTextHandler getHandler() {
    return handler;
  }
}
