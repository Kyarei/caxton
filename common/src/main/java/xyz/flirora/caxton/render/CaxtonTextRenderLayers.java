// (C) 2022-2023 +merlan #flirora.
// SPDX-License-Identifier: MIT
package xyz.flirora.caxton.render;

import com.mojang.blaze3d.systems.RenderSystem;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gl.GlUniform;
import net.minecraft.client.gl.ShaderProgramKey;
import net.minecraft.client.render.*;
import net.minecraft.util.TriState;
import xyz.flirora.caxton.CaxtonModClient;
import xyz.flirora.caxton.font.FontTech;

@Environment(EnvType.CLIENT)
public class CaxtonTextRenderLayers {
  private final Group msdf =
      new Group(
          RenderLayerFunctions::ofText,
          RenderLayerFunctions::ofTextSeeThrough,
          RenderLayerFunctions::ofTextOffset,
          RenderLayerFunctions::ofTextOutline);
  private final Group raster =
      new Group(
          RenderLayerFunctions::ofTextR,
          RenderLayerFunctions::ofTextSeeThroughR,
          RenderLayerFunctions::ofTextOffsetR,
          RenderLayerFunctions::ofTextR);
  private final Group rasterAa =
      new Group(
          RenderLayerFunctions::ofTextRA,
          RenderLayerFunctions::ofTextSeeThroughRA,
          RenderLayerFunctions::ofTextOffsetRA,
          RenderLayerFunctions::ofTextRA);

  public void clear() {
    msdf.clear();
    raster.clear();
  }

  public RenderLayer text(
      CaxtonAtlas.Page page, CaxtonTextLayerType layerType, FontTech tech, boolean blur) {
    return (switch (tech) {
      case MSDF -> msdf.text(page, layerType);
      case RASTER -> (blur ? rasterAa : raster).text(page, layerType);
    });
  }

  public static class Group {
    private final ConcurrentHashMap<CaxtonAtlas.Page, RenderLayer> text = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<CaxtonAtlas.Page, RenderLayer> textSeeThrough =
        new ConcurrentHashMap<>();
    private final ConcurrentHashMap<CaxtonAtlas.Page, RenderLayer> textOffset =
        new ConcurrentHashMap<>();
    private final ConcurrentHashMap<CaxtonAtlas.Page, RenderLayer> textOutline =
        new ConcurrentHashMap<>();
    private final Function<CaxtonAtlas.Page, RenderLayer> ofText,
        ofTextSeeThrough,
        ofTextOffset,
        ofTextOutline;

    public Group(
        Function<CaxtonAtlas.Page, RenderLayer> ofText,
        Function<CaxtonAtlas.Page, RenderLayer> ofTextSeeThrough,
        Function<CaxtonAtlas.Page, RenderLayer> ofTextOffset,
        Function<CaxtonAtlas.Page, RenderLayer> ofTextOutline) {
      this.ofText = ofText;
      this.ofTextSeeThrough = ofTextSeeThrough;
      this.ofTextOffset = ofTextOffset;
      this.ofTextOutline = ofTextOutline;
    }

    public void clear() {
      text.clear();
      textSeeThrough.clear();
      textOffset.clear();
      textOutline.clear();
    }

    public RenderLayer text(CaxtonAtlas.Page page, CaxtonTextLayerType layerType) {
      return switch (layerType) {
        case NORMAL -> text.computeIfAbsent(page, this.ofText);
        case SEE_THROUGH -> textSeeThrough.computeIfAbsent(page, this.ofTextSeeThrough);
        case OUTLINE -> textOutline.computeIfAbsent(page, this.ofTextOutline);
        case POLYGON_OFFSET -> textOffset.computeIfAbsent(page, this.ofTextOffset);
      };
    }
  }

  private static class RenderLayerFunctions extends RenderLayer {
    // not used; only here because I’m lazy
    public RenderLayerFunctions(
        String name,
        VertexFormat vertexFormat,
        VertexFormat.DrawMode drawMode,
        int expectedBufferSize,
        boolean hasCrumbling,
        boolean translucent,
        Runnable startAction,
        Runnable endAction) {
      super(
          name,
          vertexFormat,
          drawMode,
          expectedBufferSize,
          hasCrumbling,
          translucent,
          startAction,
          endAction);
    }

    private static RenderLayer make(
        String name,
        ShaderProgram shader,
        CaxtonAtlas.Page page,
        boolean seeThrough,
        boolean rasterized,
        boolean blur,
        boolean polygonOffset) {
      return RenderLayer.of(
          name,
          VertexFormats.POSITION_COLOR_TEXTURE_LIGHT,
          VertexFormat.DrawMode.QUADS,
          256,
          false,
          CaxtonModClient.CONFIG.sortTextRenderLayers,
          RenderLayer.MultiPhaseParameters.builder()
              .program(shader)
              .texture(
                  new RenderPhase.Texture(
                      page.getId(),
                      blur ? TriState.TRUE : TriState.FALSE,
                      page.getNumMipmapLevels() > 1))
              .transparency(TRANSLUCENT_TRANSPARENCY)
              .lightmap(ENABLE_LIGHTMAP)
              .depthTest(seeThrough ? RenderPhase.ALWAYS_DEPTH_TEST : RenderPhase.LEQUAL_DEPTH_TEST)
              .layering(
                  polygonOffset ? RenderPhase.POLYGON_OFFSET_LAYERING : RenderPhase.NO_LAYERING)
              .build(false));
    }

    private static RenderLayer ofText(CaxtonAtlas.Page page) {
      return make(
          "caxton_text",
          new Shayder(CaxtonShaders.TEXT_KEY, page),
          page,
          false,
          false,
          true,
          false);
    }

    private static RenderLayer ofTextSeeThrough(CaxtonAtlas.Page page) {
      return make(
          "caxton_text_see_through",
          new Shayder(CaxtonShaders.TEXT_SEE_THROUGH_KEY, page),
          page,
          true,
          false,
          true,
          false);
    }

    private static RenderLayer ofTextOffset(CaxtonAtlas.Page page) {
      return make(
          "caxton_text", new Shayder(CaxtonShaders.TEXT_KEY, page), page, false, false, true, true);
    }

    private static RenderLayer ofTextOutline(CaxtonAtlas.Page page) {
      return make(
          "caxton_text_outline",
          new Shayder(CaxtonShaders.TEXT_OUTLINE_KEY, page),
          page,
          false,
          false,
          true,
          false);
    }

    private static RenderLayer ofTextR(CaxtonAtlas.Page page) {
      return make("caxton_text_raster", TEXT_INTENSITY_PROGRAM, page, false, true, false, false);
    }

    private static RenderLayer ofTextSeeThroughR(CaxtonAtlas.Page page) {
      return make(
          "caxton_text_see_through_raster",
          TRANSPARENT_TEXT_INTENSITY_PROGRAM,
          page,
          true,
          true,
          false,
          false);
    }

    private static RenderLayer ofTextOffsetR(CaxtonAtlas.Page page) {
      return make(
          "caxton_text_outline_raster",
          TRANSPARENT_TEXT_INTENSITY_PROGRAM,
          page,
          false,
          true,
          false,
          true);
    }

    private static RenderLayer ofTextRA(CaxtonAtlas.Page page) {
      return make("caxton_text_raster", TEXT_INTENSITY_PROGRAM, page, false, true, true, false);
    }

    private static RenderLayer ofTextSeeThroughRA(CaxtonAtlas.Page page) {
      return make(
          "caxton_text_see_through_raster",
          TRANSPARENT_TEXT_INTENSITY_PROGRAM,
          page,
          true,
          true,
          true,
          false);
    }

    private static RenderLayer ofTextOffsetRA(CaxtonAtlas.Page page) {
      return make(
          "caxton_text_outline_raster",
          TRANSPARENT_TEXT_INTENSITY_PROGRAM,
          page,
          false,
          true,
          true,
          true);
    }

    public static class Shayder extends RenderPhase.ShaderProgram {
      private final ShaderProgramKey key;
      private final CaxtonAtlas.Page page;

      public Shayder(ShaderProgramKey key, CaxtonAtlas.Page page) {
        super(key);
        this.key = key;
        this.page = page;
      }

      @Override
      public void startDrawing() {
        var shader = RenderSystem.setShader(this.key);
        GlUniform unitRange = ((ShaderExt) shader).caxton$getUnitRange();
        if (unitRange != null) {
          unitRange.set(((float) page.getRange()) / CaxtonAtlas.PAGE_SIZE);
        }
      }
    }
  }
}
